package com.emroze.abdullah.ascii_counts.Model;

public class AsciiItem {
    String text;
    int count;

    public AsciiItem() {
    }

    public AsciiItem(String text, int count) {
        this.text = text;
        this.count = count;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
