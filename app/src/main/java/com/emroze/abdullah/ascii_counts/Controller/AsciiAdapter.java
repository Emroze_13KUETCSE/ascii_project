package com.emroze.abdullah.ascii_counts.Controller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emroze.abdullah.ascii_counts.Model.AsciiItem;
import com.emroze.abdullah.ascii_counts.R;

import java.util.ArrayList;

public class AsciiAdapter extends RecyclerView.Adapter<AsciiAdapter.ViewHolder>{

    public ArrayList<AsciiItem> asciiItems;
    private Context context;

    public AsciiAdapter(ArrayList<AsciiItem> asciiItems, Context context){
        this.asciiItems= asciiItems;
        this.context = context;
    }

    /**
     * The method declaration for set Histogram Data. This method will be called
     * to set data.
     *
     * @param list
     */
    public void setItemList(ArrayList<AsciiItem> list) {
        this.asciiItems = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder ViewHolder = new ViewHolder(itemView);
        return ViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AsciiAdapter.ViewHolder holder, final int position) {
        AsciiItem item = asciiItems.get(position);

        holder.tvText.setText("\""+item.getText()+"\" is "+String.valueOf(item.getCount()));
    }

    @Override
    public int getItemCount() {
        if (asciiItems != null) {
            return asciiItems.size();
        } else {
            return 0;
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvText;

        protected ViewHolder(View view) {
            super(view);

            tvText = view.findViewById(R.id.tv_text);

        }
    }
}


