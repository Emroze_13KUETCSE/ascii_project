package com.emroze.abdullah.ascii_counts;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.emroze.abdullah.ascii_counts.Controller.AsciiAdapter;
import com.emroze.abdullah.ascii_counts.Model.AsciiItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    ArrayList<AsciiItem> asciiItems = new ArrayList<>();
    AsciiAdapter asciiAdapter;
    RecyclerView recyclerView;
    protected Button buttonGenerateOutput;
    protected ProgressBar progressBar;
    private static final String TAG = "ASCII";
    ArrayList<String> dataSet = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initParam();
        initData();

    }

    @Override
    protected void onResume() {
        super.onResume();

        buttonGenerateOutput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScanAsyncTask scanAsyncTask = new ScanAsyncTask();
                scanAsyncTask.setProgressbar(progressBar);
                scanAsyncTask.execute();
            }
        });
    }

    /**
     * The method declaration for initiate the params.
     */
    private void initParam(){
        buttonGenerateOutput = findViewById(R.id.btn_generate);
        progressBar = findViewById(R.id.progress_circular);
        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
    }

    /**
     * The method declaration for initiate the Model data for histogram.
     */
    private void initData(){
        //dataSet.add("Simprints, every person counts");
        dataSet.add("Relentless Commitment to Impact");
        dataSet.add("Robust as Fudge");
        dataSet.add("Be Surprisingly Bold");
        dataSet.add("Get Back Up");
        dataSet.add("Make It Happen");
        dataSet.add("Don't Be a Jerk");
        dataSet.add("Confront the Grey");
        dataSet.add("Laugh Together, Grow Together");
    }

    /**
     * The AsyncTask declaration for OCR. This take a bitmap object
     * then process it and generate the Text output.
     *
     */
    class ScanAsyncTask extends AsyncTask<Void,Void,Boolean> {

        ProgressBar circleProgressBar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        @Override
        protected void onProgressUpdate(Void...values) {
            super.onProgressUpdate();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            for(int i = 0 ; i < dataSet.size(); i++){
                asciiItems.add(new AsciiItem(dataSet.get(i),countAscii(dataSet.get(i))));
            }

            Collections.sort(asciiItems,new SumComparator());

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            this.circleProgressBar.setVisibility(View.INVISIBLE);

            Log.v(TAG, " Output : \n");

            for(int i = 0 ; i < asciiItems.size(); i++){
                AsciiItem item = asciiItems.get(i);
                Log.v(TAG, "\""+item.getText()+"\" is "+String.valueOf(item.getCount()));

            }

            asciiAdapter = new AsciiAdapter(asciiItems,getApplicationContext());
            recyclerView.setAdapter(asciiAdapter);
        }

        private int countAscii(String str) {
            int count = 0;

            for(int i=0; i < str.length(); i++) {
                count = count+(int) str.charAt(i);
            }

            return count;
        }

        class SumComparator implements Comparator<AsciiItem> {
            public int compare(AsciiItem item1, AsciiItem item2) {

                int count1 = item1.getCount();

                int count2 = item2.getCount();

                return count2 - count1;
            }
        }


    }
}
